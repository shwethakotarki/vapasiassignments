package OnlineShopping;
import java.util.*;
import java.util.stream.Collectors;

public class input {
    cart cart = new cart();
    products prods = new products();

    private int ch = 0;
    private int innerch = 0;

    public input () {
        menu();
    }

    public void storeProductsMenu() {
        System.out.println("**********************");
        System.out.println("1. Add to Cart");
        System.out.println("2. Remove From Cart");
        System.out.println("0. Exit");
        System.out.println("**********************");
    }
    public void ownerMenu() {
        System.out.println("****************************");
        System.out.println("1. Display Store Products");
        System.out.println("2. Add a Brand");
        System.out.println("3. Add a Product");
        //System.out.println("4. Delete a Product");
        System.out.println("0. Exit");
        System.out.println("****************************");
    }
    public void userMenu() {
        System.out.println("****************************");
        System.out.println("1. Display Store Products");
        System.out.println("2. Filter by Clothing Type");
        System.out.println("3. Filter by Accessories Type");
       // System.out.println("4. Filter the Products by Brand");
        System.out.println("4. Show the Cart");
        System.out.println("0. Exit");
        System.out.println("****************************");
    }

    public void menu () {

        System.out.println("Enter the username");
        System.out.println("___________________");
        Scanner in = new Scanner (System.in);
        String uname = in.nextLine();

      //  do {
            String strAdmin = "admin";
            if(uname.compareTo(strAdmin)==0)
            {
                ownerMenu();
                getUserInput();
                ownerChoices();
            }
            else
            {
                do {
                    userMenu();
                    getUserInput();
                    userChoices();
                }while (ch != 0);
            }
     //   } while (ch != 0);
    }

    private void ownerChoices()
    {
        do {

            switch (ch) {

                case 1:
                    displayStoreProducts();
                    ownerMenu();
                    getUserInput();
                    break;
                case 2:
                    System.out.println("Enter a Brand Name");
                    Scanner inp = new Scanner(System.in);
                    String bName = inp.nextLine();
                    addBrand(bName);
                    ownerMenu();
                    getUserInput();
                    break;
                case 3:
                    System.out.println("Enter product name,product quantity)(Eg. Hair clips,2)");
                    Scanner pr = new Scanner(System.in);
                    String pname_qty = pr.nextLine();
                    String pname = pname_qty.split(",")[0];
                    Integer qty = Integer.parseInt(pname_qty.split(",")[1]);
                    System.out.println("Choose from a list of available product types)(Eg. Clothes)");
                    System.out.println(productType.Clothes);
                    System.out.println(productType.Accessories);
                    Scanner type = new Scanner(System.in);
                    String producttype = type.nextLine();
                    System.out.println("Choose from a list of available Brands)(Eg. 1)");
                    displayStoreBrands(null);
                    Scanner br = new Scanner(System.in);
                    String brId = br.nextLine();
                    addProduct(pname, qty, producttype, Integer.parseInt(brId));
                    displayStoreProducts();
                    ownerMenu();
                    getUserInput();
                    break;
                case 0:
                    System.exit(0);
                    break;
                default:
                    break;
            }
        } while (ch != 0);
    }

    private void userChoices()
    {
       // do {
            switch (ch) {

                case 1:
                    displayStoreProducts();
                    storeProductsMenu();
                    getUserInput();
                    innerChoice1();

                    break;
                case 2:
                    displayProductsByProdType(productType.Clothes.toString());
                    userMenu();
                    getUserInput();
                    //innerChoice1();
                    break;
                case 3:
                    displayProductsByProdType(productType.Accessories.toString());
                    userMenu();
                    getUserInput();
                    //innerChoice1();
                    break;
                //case 4:
                //  displayProdsByBrand("BIBA");
                // userMenu();
                // getUserInput();
                // break;
                case 4:
                    showCart();
                    break;
                case 0:
                    System.exit(0);
                    break;
                default:
                    break;
            }
        //} while (ch != 0);
    }
    private void innerChoice1() {
        switch (ch) {
            case 1:
                addProductToCart();
                showCart();
                break;
            case 2:
                removeProductFromCart();
                break;
            default:
                break;
        }
    }

    private int getUserInput() throws NumberFormatException {
        Scanner in = new Scanner (System.in);
        ch = Integer.parseInt(in.nextLine());
        return ch;
    }

    private void displayStoreProducts() {
        List<product> products = new products().getProducts();
        if(products.size()>0)
            System.out.println("LIST OF AVAILABLE PRODUCTS");
        for (product prod: products) {
            System.out.println(
                    prod.getPid() + "- " +
                            prod.getName() + " \t\t" +
                            prod.getQty() + " \t\t" +
                            prod.getType() + " \t\t" + prod.getBrand()
            );
        }
    }

    public void displayProductsByProdType(String type) {
        List<product> productsByType = new ArrayList<product>();
        productsByType =prods.getProductsByType(type);
        if (productsByType.size() >0) {
            System.out.println("LIST OF THE PRODUCTS OF TYPE " + type);
            for (product p : productsByType) {
                if (p.getType() == type) {
                    System.out.println(p.getName() + "\t" +
                            p.getQty() + "\t" +
                            p.getBrand());
                }
            }

        }
    }

    private void displayStoreBrands(List<brand> brandList) {
        List<brand> brands = new ArrayList<brand>();

        if(brandList == null)
             brands = new Brands().getBrands();
        else
            brands = brandList;


        brands.stream().collect(Collectors.groupingBy(p -> p.getbName())).values().
                forEach(t -> System.out.println(t.get(0).getbId() + " \t " + t.get(0).getbName()));

        //System.out.println(brands.stream().distinct().map(brand->brand.getbName()).collect(Collectors.toList()));
        /*for (brand br : brands) {
            //if(br.getbName().)
            System.out.println(
                    br.getbId() + "- " +
                            br.getbName()
            )
        }*/
    }

    private void displayProdsByBrand(String brandName)
    {
        List<product> prodByBrand =  new ArrayList<product>();
        prodByBrand = prods.getProductsByBrand(brandName);
        for (product p :prodByBrand)
        {
            System.out.println(p.getName());
            System.out.println(p.getQty());
            System.out.println(p.getType());
        }

    }

    private void addProduct(String productName,Integer qty,String type,Integer brandID)
    {
        List<product> products = new ArrayList<product>() ;
        products = prods.addProduct(productName,qty,type,brandID);
        Integer prodCount = Math.toIntExact(products.stream().filter(prod -> prod.getName() == productName).count());
        if(prodCount>0)
            System.out.println("Product with name :" + productName + " added successfully");

    }
    private void addBrand(String bName) {
        List<brand> brands = new ArrayList<brand>();
        Brands br = new Brands();
        brands= br.addBrands(bName);
        Integer brandCount = Math.toIntExact(brands.stream().filter(brand -> brand.getbName() == bName).count());
        if(brandCount>0)
        {
            System.out.println("Brand added successfully");
        }
        displayStoreBrands(brands);

    }
    private void addProductToCart() {
        int pid = getUserInput();
        cart.addproductToCartByPID(pid);
    }

    private void showCart() {
        cart.printCartItems();
    }

    private void removeProductFromCart() {
        int pid = getUserInput();
        cart.removeproductByPID(pid);
    }
}
