package OnlineShopping;

import java.util.ArrayList;
import java.util.List;

public class Brands {
    private static final List<brand> brands = new ArrayList<>();

    public Brands () {
        if(brands.size()==0)
            this.brandsList();
    }
    public void brandsList() {
        //String[] productNames = {"Kurtis", "Leggings", "Dupatta", "Belts"};
        //Integer[] qty = {5, 6, 7, 3};
        //String[] type = {"Clothing", "Clothing", "Clothing", "Accessories"};
        String[] brandNames = {"BIBA", "MAX", "ZARA", "BIBA"};

        for (int i = 0; i < brandNames.length; i++) {
            this.brands.add(new brand(i+1,brandNames[i]));
        }
    }
    public List<brand> getBrands() {
        return brands;
    }
    public List<brand> addBrands(String bName) {
        brands.add(new brand(brands.size()+1,bName));
        return brands;
    }
    public String getBrandName(Integer bId) {
        List<brand> brands = new Brands().getBrands();
        String brandName = "" ;
        for (brand br : brands) {
            if (br.getbId() == bId) {
                brandName = br.getbName();
                break;
            }
        }
        return brandName;

    }
    }


