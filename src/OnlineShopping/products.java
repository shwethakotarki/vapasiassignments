package OnlineShopping;
import OnlineShopping.product;
import OnlineShopping.product;

import java.util.*;

public class products {
    private static final List<product> products = new ArrayList<product>();

    public products () {
        if(products.size()==0)
            this.defaultStoreItems();
    }
    public void defaultStoreItems() {
        String[] productNames = {"Kurtis", "Leggings", "Dupatta", "Belts"};
        Integer[] qty = {5, 6, 7, 3};
        String[] type = {"Clothes", "Clothes", "Clothes", "Accessories"};
        String[] brandNames = {"BIBA", "MAX", "ZARA", "BIBA"};

        for (int i = 0; i < productNames.length; i++) {
            this.products.add(new product(i + 1, productNames[i], qty[i], type[i], brandNames[i]));
        }
    }

    public List<product> getProducts() {
        return products;
    }

    public List<product> addProduct(String productName,Integer qty,String type,Integer brandID) {

        Brands br = new Brands();
        this.products.add(new product(products.size() + 1, productName, qty, type, br.getBrandName(brandID)));
        return products;
    }

    public List<product> getProductsByBrand(String brandName) {
        List<product> productsByBrand = new ArrayList<product>();
            if (products != null) {
                for (product p : products) {
                    if (p.getBrand() == brandName) {
                        productsByBrand.add(p);
                        //System.out.println(p.getName());
                        //System.out.println(p.getQty());
                        //System.out.println(p.getType());
                    }
                }

            }

        return productsByBrand;
    }

    public List<product> getProductsByType(String type) {
        List<product> productsByType = new ArrayList<product>();
        if (products != null) {
            for (product p : products) {
                if (p.getType() == type) {
                    productsByType.add(p);
                }
            }

        }

        return productsByType;
    }

    public product getproductByproductID(int pid) {
        product product = null;
        List<product> products = new products().getProducts();
        for (product prod: products) {
            if (prod.getPid() == pid) {
                product = prod;
                break;
            }
        }
        return product;
    }



}
