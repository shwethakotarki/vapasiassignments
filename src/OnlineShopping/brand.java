package OnlineShopping;

import java.util.ArrayList;
import java.util.List;

public class brand{

    private Integer bId;
    private String bName;



    public brand () {
    }

    public brand (Integer bId, String bName) {
        this.bId = bId;
        this.bName = bName;
    }

    public Integer getbId() {
        return bId;
    }

    public String getbName() {
        return bName;
    }
}
