package OnlineShopping;

import OnlineShopping.products;

import java.util.ArrayList;
import java.util.List;

public class cart {

    List<product> cartItems = new ArrayList<product>();
    products prods = new products();

    public void addproductToCartByPID(int pid) {
        product product = prods.getproductByproductID(pid);
        addToCart(product);
    }

    private void addToCart(product product) {
        cartItems.add(product);
        System.out.println("Added " + product.getName() + " to the cart");
    }

    void printCartItems() {
        for (product prod: cartItems) {
            System.out.println(prod.getName() + "\t " + prod.getType()+" \t " + prod.getBrand());

        }
    }
    public void removeproductByPID(int pid) {

        product prod = prods.getproductByproductID(pid);
        cartItems.remove(prod);
        System.out.println("Removed " + prod.getName() + " from the cart");
    }

}
