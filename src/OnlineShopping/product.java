package OnlineShopping;

public class product {
    private Integer pid;
    private String name;
    private int qty;
    private String type;
    private String brand;


    public product(Integer pid, String name, int qty, String type, String brand) {
        this.pid = pid;
        this.name = name;
        this.qty = qty;
        this.type = type;
        this.brand = brand;

    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

}
