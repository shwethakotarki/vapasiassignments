package JavaBasics;

public class TwoDArrayOpns {

    public static void main(String[] args)
    {
        int[][] matrix1 = {{1,2},{3,4},{5,6}};//2 dimensional array of [3][2]
        int[][] matrix2 = {{7,8},{9,10},{11,12}};

        int[][] matrix3 = {{7,8,9},{2,10,11},{11,12,13}};
        int[][] matrix4 = {{6,6,7},{2,4,11},{5,12,9}};

        int[][] sum = new int[3][2];
        int[][] product = new int[3][3];

        System.out.print("Addition Result"+"\n");
        //Matrix Addition
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                sum[i][j] = matrix1[i][j] + matrix2[i][j];

                System.out.print(sum[i][j]+"\t");
            }

            System.out.println();
        }

        //Matrix multiplication
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                for (int k = 0; k < 3; k++)
                {
                    product[i][j] +=  matrix3[i][k] * matrix4[k][j];
                }
            }
        }

        System.out.print("Multiplication Result"+"\n");
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {

                System.out.print(product[i][j]+"\t");
            }

            System.out.println();
        }
    }


}
