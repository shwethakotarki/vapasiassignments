package JavaBasics;

public class PalindromeNumber {
    public static void main(String args[]){
        int r,sum=0,temp;
        //int n=1234523;
        int n=1234321;

        temp=n;
        while(n>0){
            r=n%10;
            sum=(sum*10)+r;
            n=n/10;
        }
        if(temp==sum)
            System.out.println("True");
        else
            System.out.println("False");
    }
}
