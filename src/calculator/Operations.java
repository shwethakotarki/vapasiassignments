package calculator;

public interface Operations {
    double compute(double operand1, double operand2);
}