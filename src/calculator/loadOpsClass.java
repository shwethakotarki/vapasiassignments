package calculator;

import java.util.HashMap;
import java.util.Map;


public class loadOpsClass {
    private Map<Character, Operations> operationMap = new HashMap<>();
    public loadOpsClass()
    {

    }

    double calculate(double operand1, double operand2, char operator) {

        switch (operator)
        {
            case '+':
                operationMap.put('+', new Addition());
            case '-':
                operationMap.put('-', new Subtraction());
            case '*':
                operationMap.put('*', new Multiplication());
            case '/':
                operationMap.put('/', new Division());

        }

        Operations operationMapValue = operationMap.get(operator);
        return operationMapValue.compute(operand1, operand2);
    }
}
